#include <QtTest>
#include <underline.h>
#include "student.h"

class Example : public QObject
{
    Q_OBJECT

public:
    Example();
    ~Example();

private slots:
    void initTestCase();
    void cleanupTestCase();

    void part01_01_serialize_student_by_custom_function();
    void part01_02_serialize_student_by_metaObject();
    void part01_03_serialize_student_by_underline();

};

Example::Example()
{

}

Example::~Example()
{

}

void Example::initTestCase()
{

}

void Example::cleanupTestCase()
{

}

void Example::part01_01_serialize_student_by_custom_function()
{
    auto jsonStringify = [](const QVariantMap& data) {
        QJsonObject jsonObject = QJsonObject::fromVariantMap(data);

        QJsonDocument doc;
        doc.setObject(jsonObject);
        QJsonDocument::JsonFormat format = QJsonDocument::Compact;
        QByteArray bytes = doc.toJson(format);
        return bytes;
    };

    auto convert = [](const Student& student) {
        QVariantMap data;
        data["id"] = student.id;
        data["name"] = student.name;
        data["year"] = student.year;
        return data;
    };

    Student student{"001", "Ben", 1};
    QVariantMap data = convert(student);

    QCOMPARE(jsonStringify(data), "{\"id\":\"001\",\"name\":\"Ben\",\"year\":1}");
}

void Example::part01_02_serialize_student_by_metaObject()
{
    auto jsonStringify = [](const QVariantMap& data) {
        QJsonObject jsonObject = QJsonObject::fromVariantMap(data);

        QJsonDocument doc;
        doc.setObject(jsonObject);
        QJsonDocument::JsonFormat format = QJsonDocument::Compact;
        QByteArray bytes = doc.toJson(format);
        return bytes;
    };

    auto convert = [](const Student& student) {
        QVariantMap data;
        const QMetaObject metaObject = Student::staticMetaObject;

        for (int i = 0 ; i < metaObject.propertyCount(); i++) {
            const QMetaProperty property = metaObject.property(i);
            QString key = property.name();
            QVariant value = property.readOnGadget(&student);
            data[key] = value;
        }

        return data;
    };

    Student student{"001", "Ben", 1};
    QVariantMap data = convert(student);

    QCOMPARE(jsonStringify(data), "{\"id\":\"001\",\"name\":\"Ben\",\"year\":1}");
}

void Example::part01_03_serialize_student_by_underline()
{
    QVariantMap data;
    Student student{"001", "Ben", 1};

    _::merge(data, student);

    QCOMPARE(_::stringify(data), "{\"id\":\"001\",\"name\":\"Ben\",\"year\":1}");
}

QTEST_APPLESS_MAIN(Example)

#include "tst_example.moc"
